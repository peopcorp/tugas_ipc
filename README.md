# Tugas_IPC

Sebuah folder tugas Sistem Terdistribusi untuk pertemuan minggu ke-9, berisi
sebuah file RAR yang didalamnya terdapat file code python untuk tugas 1-4
mengenai client dan server. Tugas tersebut terdiri dari:

1. File Code Tugas 1 TCP Client dan Server
2. File Code Tugas 2 UDP Client dan Server
3. File Code Tugas 3 Upload-File Client dan Server
4. File Code Tugas 4 Download-File Client dan Server

serta beberapa file lainnya seperti file txt yang terdiri dari:
1. file_didownload
2. file_diupload
3. hasil_download
4. hasil_upload

Untuk beberapa file terakhir lainnya yaitu dari file docx, terdiri dari:
1. IPC_IFIK-40-04_Kelompok 3 (Dokumentasi Screenshot Hasil Running Program)
2. tugas_ipc (File Soal yang ditugaskan)